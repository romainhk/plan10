import datetime
import gitlab
import os

from flask import Flask, render_template

from config import GITLAB_URL, GITLAB_TOKEN, GITLAB_PROJECTS


class MergeRequest(object):

    """
    Merge request representation.
    """

    def __init__(
            self,
            iid,
            title,
            state,
            author,
            assignee,
            work_in_progress,
            merge_status,
            web_url,
            milestone,
            updated_at,
            *args,
            **kwargs
            ):
        self.iid = iid
        self.title = title
        self.state = state
        self.author = author
        self.assignee = assignee
        self.work_in_progress = work_in_progress
        self.merge_status = merge_status
        self.web_url = web_url
        self.milestone = milestone
        self._updated_at = updated_at

    @property
    def updated_at(self):
        return datetime.datetime.strptime(self._updated_at, '%Y-%m-%dT%H:%M:%S.%fZ')


class Plan10(object):
    """
    """

    def __init__(self, url, token, projects):
        self.gitlab = gitlab.Gitlab(url, private_token=token)
        self.followed_projects = projects
        self._projects = None

    @property
    def projects(self):
        if self._projects is None:
            self._projects = self.list_projects()

        return self._projects

    @property
    def projects_dict(self):
        return {x.id: x for x in self.projects}

    def list_projects(self):
        """
        :rtype: list
        """
        projects = []
        for project in self.gitlab.projects.list(all=True,
                                                 order_by="name",
                                                 sort="asc"):
            if project.path_with_namespace in self.followed_projects:
                projects.append(project)

        return projects

    def list_merge_requests(self, project_id):
        """
        :rtype: list of MergeRequest
        """
        if project_id in self.projects_dict:
            mrs = self.projects_dict[project_id].mergerequests.list(
                all=True, state="opened", order_by="updated_at")
            return [MergeRequest(**mr._attrs) for mr in mrs]

        return []

    def list_mr_by_project(self):
        """
        :rtype: dict
        """
        mrs = {}
        for project in self.projects:
            mrs[project.id] = self.list_merge_requests(project.id)

        return mrs


def create_app(test_config=None):
    # create and configure the app
    app = Flask(
        __name__,
        instance_relative_config=True
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    plan10 = Plan10(
        url=GITLAB_URL,
        token=GITLAB_TOKEN,
        projects=GITLAB_PROJECTS,
    )

    @app.route('/')
    def index():
        plan10.list_projects()
        merge_requests = plan10.list_mr_by_project()

        return render_template(
            'index.html',
            projects=plan10.projects,
            merge_requests=merge_requests
        )

    return app
